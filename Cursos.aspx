﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="Cursos.aspx.cs" Inherits="Cursos" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="contenido" Runat="Server">
    <asp:GridView ID="GridView1" runat="server" AllowPaging="True" AllowSorting="True" AutoGenerateColumns="False" BackColor="White" BorderColor="#336666" BorderStyle="Double" BorderWidth="3px" CellPadding="4" DataKeyNames="CodCur" DataSourceID="LinqDataSource1" GridLines="Horizontal">
        <Columns>
            <asp:CommandField ShowSelectButton="True" />
            <asp:BoundField DataField="CodCur" HeaderText="CodCur" InsertVisible="False" ReadOnly="True" SortExpression="CodCur" />
            <asp:BoundField DataField="nombre" HeaderText="nombre" SortExpression="nombre" />
            <asp:BoundField DataField="dniP" HeaderText="dniP" SortExpression="dniP" />
            <asp:BoundField DataField="Profesor.nombre" HeaderText="Profesor" SortExpression="Profesor" />
        </Columns>
        <FooterStyle BackColor="White" ForeColor="#333333" />
        <HeaderStyle BackColor="#336666" Font-Bold="True" ForeColor="White" />
        <PagerStyle BackColor="#336666" ForeColor="White" HorizontalAlign="Center" />
        <RowStyle BackColor="White" ForeColor="#333333" />
        <SelectedRowStyle BackColor="#339966" Font-Bold="True" ForeColor="White" />
        <SortedAscendingCellStyle BackColor="#F7F7F7" />
        <SortedAscendingHeaderStyle BackColor="#487575" />
        <SortedDescendingCellStyle BackColor="#E5E5E5" />
        <SortedDescendingHeaderStyle BackColor="#275353" />
    </asp:GridView>
    <asp:LinqDataSource ID="LinqDataSource1" runat="server" ContextTypeName="DataClassesDataContext" EntityTypeName="" TableName="Curso">
    </asp:LinqDataSource>
    <asp:LinqDataSource ID="LinqDataSource2" runat="server" ContextTypeName="DataClassesDataContext" EnableDelete="True" EnableInsert="True" EnableUpdate="True" EntityTypeName="" TableName="Curso" Where="CodCur == @CodCur">
        <WhereParameters>
            <asp:ControlParameter ControlID="GridView1" DefaultValue="1" Name="CodCur" PropertyName="SelectedValue" Type="Int32" />
        </WhereParameters>
    </asp:LinqDataSource>
    <asp:FormView ID="FormView1" runat="server" DataKeyNames="CodCur" DataSourceID="LinqDataSource2">
        <EditItemTemplate>
            CodCur:
            <asp:Label ID="CodCurLabel1" runat="server" Text='<%# Eval("CodCur") %>' />
            <br />
            nombre:
            <asp:TextBox ID="nombreTextBox" runat="server" Text='<%# Bind("nombre") %>' />
            <br />
            Profesor:
            <asp:DropDownList ID="DropDownList1" runat="server" DataSourceID="LinqDataSource3" DataTextField="nombre" DataValueField="dni" SelectedValue='<%# Bind("dniP") %>'>
            </asp:DropDownList>
            <asp:LinqDataSource ID="LinqDataSource3" runat="server" ContextTypeName="DataClassesDataContext" EntityTypeName="" TableName="Profesor">
            </asp:LinqDataSource>
            <br />
            <asp:LinkButton ID="UpdateButton" runat="server" CausesValidation="True" CommandName="Update" Text="Actualizar" />
            &nbsp;<asp:LinkButton ID="UpdateCancelButton" runat="server" CausesValidation="False" CommandName="Cancel" Text="Cancelar" />
        </EditItemTemplate>
        <InsertItemTemplate>
            nombre:
            <asp:TextBox ID="nombreTextBox" runat="server" Text='<%# Bind("nombre") %>' />
            <br />
            Profesor:
            <asp:DropDownList ID="DropDownList2" runat="server" DataSourceID="LinqDataSource4" DataTextField="nombre" DataValueField="dni" SelectedValue='<%# Bind("dniP") %>'>
            </asp:DropDownList>
            <asp:LinqDataSource ID="LinqDataSource4" runat="server" ContextTypeName="DataClassesDataContext" EntityTypeName="" TableName="Profesor">
            </asp:LinqDataSource>
            <br />
            <asp:LinkButton ID="InsertButton" runat="server" CausesValidation="True" CommandName="Insert" Text="Insertar" />
&nbsp;<asp:LinkButton ID="InsertCancelButton" runat="server" CausesValidation="False" CommandName="Cancel" Text="Cancelar" />
        </InsertItemTemplate>
        <ItemTemplate>
            CodCur:
            <asp:Label ID="CodCurLabel" runat="server" Text='<%# Eval("CodCur") %>' />
            <br />
            nombre:
            <asp:Label ID="nombreLabel" runat="server" Text='<%# Bind("nombre") %>' />
            <br />
            dniP:
            <asp:Label ID="dniPLabel" runat="server" Text='<%# Bind("dniP") %>' />
            <br />
            matCur:
            <asp:Label ID="matCurLabel" runat="server" Text='<%# Bind("matCur") %>' />
            <br />
            Profesor:
            <asp:Label ID="ProfesorLabel" runat="server" Text='<%# Bind("Profesor") %>' />
            <br />
            <asp:LinkButton ID="EditButton" runat="server" CausesValidation="False" CommandName="Edit" Text="Editar" />
            &nbsp;<asp:LinkButton ID="DeleteButton" runat="server" CausesValidation="False" CommandName="Delete" Text="Eliminar" />
            &nbsp;<asp:LinkButton ID="NewButton" runat="server" CausesValidation="False" CommandName="New" Text="Nuevo" />
        </ItemTemplate>
    </asp:FormView>
</asp:Content>

