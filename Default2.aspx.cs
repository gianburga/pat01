﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Default2 : System.Web.UI.Page
{
    DataClassesDataContext data = new DataClassesDataContext();
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack){
            var cursos = data.Curso.ToList();
            DropDownList1.DataSource = cursos;
            DropDownList1.DataTextField = "nombre";
            DropDownList1.DataValueField = "nombre";
            DropDownList1.DataBind();
            DropDownList1.Items.Insert(0, "Mostrar todo");

            GridView1.DataSource = data.alumnos_curso("");
            GridView1.DataBind();
            TextBox1.Text = GridView1.Rows.Count.ToString();
        }
    }

    protected void DropDownList1_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (DropDownList1.SelectedIndex != 0)
        {
            var alumnos = data.alumnos_curso(DropDownList1.SelectedValue);
            TextBox1.Text = data.cantidad_alumnos_curso(DropDownList1.SelectedValue).ToString();
            GridView1.DataSource = alumnos;
            GridView1.DataBind();
        }
        else
        {
            GridView1.DataSource = data.alumnos_curso("");
            GridView1.DataBind();
            TextBox1.Text = GridView1.Rows.Count.ToString();
        }
    }
}