﻿#pragma warning disable 1591
//------------------------------------------------------------------------------
// <auto-generated>
//     Este código fue generado por una herramienta.
//     Versión de runtime:4.0.30319.18444
//
//     Los cambios en este archivo podrían causar un comportamiento incorrecto y se perderán si
//     se vuelve a generar el código.
// </auto-generated>
//------------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.Linq;
using System.Data.Linq.Mapping;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;



[global::System.Data.Linq.Mapping.DatabaseAttribute(Name="PAT05")]
public partial class DataClassesDataContext : System.Data.Linq.DataContext
{
	
	private static System.Data.Linq.Mapping.MappingSource mappingSource = new AttributeMappingSource();
	
  #region Definiciones de métodos de extensibilidad
  partial void OnCreated();
  partial void InsertCurso(Curso instance);
  partial void UpdateCurso(Curso instance);
  partial void DeleteCurso(Curso instance);
  partial void InsertProfesor(Profesor instance);
  partial void UpdateProfesor(Profesor instance);
  partial void DeleteProfesor(Profesor instance);
  partial void InsertmatCur(matCur instance);
  partial void UpdatematCur(matCur instance);
  partial void DeletematCur(matCur instance);
  partial void InsertMatricula(Matricula instance);
  partial void UpdateMatricula(Matricula instance);
  partial void DeleteMatricula(Matricula instance);
  #endregion
	
	public DataClassesDataContext() : 
			base(global::System.Configuration.ConfigurationManager.ConnectionStrings["PAT05ConnectionString"].ConnectionString, mappingSource)
	{
		OnCreated();
	}
	
	public DataClassesDataContext(string connection) : 
			base(connection, mappingSource)
	{
		OnCreated();
	}
	
	public DataClassesDataContext(System.Data.IDbConnection connection) : 
			base(connection, mappingSource)
	{
		OnCreated();
	}
	
	public DataClassesDataContext(string connection, System.Data.Linq.Mapping.MappingSource mappingSource) : 
			base(connection, mappingSource)
	{
		OnCreated();
	}
	
	public DataClassesDataContext(System.Data.IDbConnection connection, System.Data.Linq.Mapping.MappingSource mappingSource) : 
			base(connection, mappingSource)
	{
		OnCreated();
	}
	
	public System.Data.Linq.Table<Curso> Curso
	{
		get
		{
			return this.GetTable<Curso>();
		}
	}
	
	public System.Data.Linq.Table<Profesor> Profesor
	{
		get
		{
			return this.GetTable<Profesor>();
		}
	}
	
	public System.Data.Linq.Table<matCur> matCur
	{
		get
		{
			return this.GetTable<matCur>();
		}
	}
	
	public System.Data.Linq.Table<Matricula> Matricula
	{
		get
		{
			return this.GetTable<Matricula>();
		}
	}
	
	public System.Data.Linq.Table<AlumnosCursosProfesores> AlumnosCursosProfesores
	{
		get
		{
			return this.GetTable<AlumnosCursosProfesores>();
		}
	}
	
	[global::System.Data.Linq.Mapping.FunctionAttribute(Name="dbo.alumnos_curso")]
	public ISingleResult<alumnos_cursoResult> alumnos_curso([global::System.Data.Linq.Mapping.ParameterAttribute(DbType="NChar(20)")] string sigla)
	{
		IExecuteResult result = this.ExecuteMethodCall(this, ((MethodInfo)(MethodInfo.GetCurrentMethod())), sigla);
		return ((ISingleResult<alumnos_cursoResult>)(result.ReturnValue));
	}
	
	[global::System.Data.Linq.Mapping.FunctionAttribute(Name="dbo.cantidad_alumnos_curso", IsComposable=true)]
	public System.Nullable<int> cantidad_alumnos_curso([global::System.Data.Linq.Mapping.ParameterAttribute(DbType="NChar(20)")] string sigla)
	{
		return ((System.Nullable<int>)(this.ExecuteMethodCall(this, ((MethodInfo)(MethodInfo.GetCurrentMethod())), sigla).ReturnValue));
	}
}

[global::System.Data.Linq.Mapping.TableAttribute(Name="dbo.Curso")]
public partial class Curso : INotifyPropertyChanging, INotifyPropertyChanged
{
	
	private static PropertyChangingEventArgs emptyChangingEventArgs = new PropertyChangingEventArgs(String.Empty);
	
	private int _CodCur;
	
	private string _nombre;
	
	private string _dniP;
	
	private EntitySet<matCur> _matCur;
	
	private EntityRef<Profesor> _Profesor;
	
    #region Definiciones de métodos de extensibilidad
    partial void OnLoaded();
    partial void OnValidate(System.Data.Linq.ChangeAction action);
    partial void OnCreated();
    partial void OnCodCurChanging(int value);
    partial void OnCodCurChanged();
    partial void OnnombreChanging(string value);
    partial void OnnombreChanged();
    partial void OndniPChanging(string value);
    partial void OndniPChanged();
    #endregion
	
	public Curso()
	{
		this._matCur = new EntitySet<matCur>(new Action<matCur>(this.attach_matCur), new Action<matCur>(this.detach_matCur));
		this._Profesor = default(EntityRef<Profesor>);
		OnCreated();
	}
	
	[global::System.Data.Linq.Mapping.ColumnAttribute(Storage="_CodCur", AutoSync=AutoSync.OnInsert, DbType="Int NOT NULL IDENTITY", IsPrimaryKey=true, IsDbGenerated=true)]
	public int CodCur
	{
		get
		{
			return this._CodCur;
		}
		set
		{
			if ((this._CodCur != value))
			{
				this.OnCodCurChanging(value);
				this.SendPropertyChanging();
				this._CodCur = value;
				this.SendPropertyChanged("CodCur");
				this.OnCodCurChanged();
			}
		}
	}
	
	[global::System.Data.Linq.Mapping.ColumnAttribute(Storage="_nombre", DbType="NVarChar(50)")]
	public string nombre
	{
		get
		{
			return this._nombre;
		}
		set
		{
			if ((this._nombre != value))
			{
				this.OnnombreChanging(value);
				this.SendPropertyChanging();
				this._nombre = value;
				this.SendPropertyChanged("nombre");
				this.OnnombreChanged();
			}
		}
	}
	
	[global::System.Data.Linq.Mapping.ColumnAttribute(Storage="_dniP", DbType="NVarChar(8)")]
	public string dniP
	{
		get
		{
			return this._dniP;
		}
		set
		{
			if ((this._dniP != value))
			{
				if (this._Profesor.HasLoadedOrAssignedValue)
				{
					throw new System.Data.Linq.ForeignKeyReferenceAlreadyHasValueException();
				}
				this.OndniPChanging(value);
				this.SendPropertyChanging();
				this._dniP = value;
				this.SendPropertyChanged("dniP");
				this.OndniPChanged();
			}
		}
	}
	
	[global::System.Data.Linq.Mapping.AssociationAttribute(Name="Curso_matCur", Storage="_matCur", ThisKey="CodCur", OtherKey="codcur")]
	public EntitySet<matCur> matCur
	{
		get
		{
			return this._matCur;
		}
		set
		{
			this._matCur.Assign(value);
		}
	}
	
	[global::System.Data.Linq.Mapping.AssociationAttribute(Name="Profesor_Curso", Storage="_Profesor", ThisKey="dniP", OtherKey="dni", IsForeignKey=true)]
	public Profesor Profesor
	{
		get
		{
			return this._Profesor.Entity;
		}
		set
		{
			Profesor previousValue = this._Profesor.Entity;
			if (((previousValue != value) 
						|| (this._Profesor.HasLoadedOrAssignedValue == false)))
			{
				this.SendPropertyChanging();
				if ((previousValue != null))
				{
					this._Profesor.Entity = null;
					previousValue.Curso.Remove(this);
				}
				this._Profesor.Entity = value;
				if ((value != null))
				{
					value.Curso.Add(this);
					this._dniP = value.dni;
				}
				else
				{
					this._dniP = default(string);
				}
				this.SendPropertyChanged("Profesor");
			}
		}
	}
	
	public event PropertyChangingEventHandler PropertyChanging;
	
	public event PropertyChangedEventHandler PropertyChanged;
	
	protected virtual void SendPropertyChanging()
	{
		if ((this.PropertyChanging != null))
		{
			this.PropertyChanging(this, emptyChangingEventArgs);
		}
	}
	
	protected virtual void SendPropertyChanged(String propertyName)
	{
		if ((this.PropertyChanged != null))
		{
			this.PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
		}
	}
	
	private void attach_matCur(matCur entity)
	{
		this.SendPropertyChanging();
		entity.Curso = this;
	}
	
	private void detach_matCur(matCur entity)
	{
		this.SendPropertyChanging();
		entity.Curso = null;
	}
}

[global::System.Data.Linq.Mapping.TableAttribute(Name="dbo.Profesor")]
public partial class Profesor : INotifyPropertyChanging, INotifyPropertyChanged
{
	
	private static PropertyChangingEventArgs emptyChangingEventArgs = new PropertyChangingEventArgs(String.Empty);
	
	private string _dni;
	
	private string _nombre;
	
	private EntitySet<Curso> _Curso;
	
    #region Definiciones de métodos de extensibilidad
    partial void OnLoaded();
    partial void OnValidate(System.Data.Linq.ChangeAction action);
    partial void OnCreated();
    partial void OndniChanging(string value);
    partial void OndniChanged();
    partial void OnnombreChanging(string value);
    partial void OnnombreChanged();
    #endregion
	
	public Profesor()
	{
		this._Curso = new EntitySet<Curso>(new Action<Curso>(this.attach_Curso), new Action<Curso>(this.detach_Curso));
		OnCreated();
	}
	
	[global::System.Data.Linq.Mapping.ColumnAttribute(Storage="_dni", DbType="NVarChar(8) NOT NULL", CanBeNull=false, IsPrimaryKey=true)]
	public string dni
	{
		get
		{
			return this._dni;
		}
		set
		{
			if ((this._dni != value))
			{
				this.OndniChanging(value);
				this.SendPropertyChanging();
				this._dni = value;
				this.SendPropertyChanged("dni");
				this.OndniChanged();
			}
		}
	}
	
	[global::System.Data.Linq.Mapping.ColumnAttribute(Storage="_nombre", DbType="NVarChar(50)")]
	public string nombre
	{
		get
		{
			return this._nombre;
		}
		set
		{
			if ((this._nombre != value))
			{
				this.OnnombreChanging(value);
				this.SendPropertyChanging();
				this._nombre = value;
				this.SendPropertyChanged("nombre");
				this.OnnombreChanged();
			}
		}
	}
	
	[global::System.Data.Linq.Mapping.AssociationAttribute(Name="Profesor_Curso", Storage="_Curso", ThisKey="dni", OtherKey="dniP")]
	public EntitySet<Curso> Curso
	{
		get
		{
			return this._Curso;
		}
		set
		{
			this._Curso.Assign(value);
		}
	}
	
	public event PropertyChangingEventHandler PropertyChanging;
	
	public event PropertyChangedEventHandler PropertyChanged;
	
	protected virtual void SendPropertyChanging()
	{
		if ((this.PropertyChanging != null))
		{
			this.PropertyChanging(this, emptyChangingEventArgs);
		}
	}
	
	protected virtual void SendPropertyChanged(String propertyName)
	{
		if ((this.PropertyChanged != null))
		{
			this.PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
		}
	}
	
	private void attach_Curso(Curso entity)
	{
		this.SendPropertyChanging();
		entity.Profesor = this;
	}
	
	private void detach_Curso(Curso entity)
	{
		this.SendPropertyChanging();
		entity.Profesor = null;
	}
}

[global::System.Data.Linq.Mapping.TableAttribute(Name="dbo.matCur")]
public partial class matCur : INotifyPropertyChanging, INotifyPropertyChanged
{
	
	private static PropertyChangingEventArgs emptyChangingEventArgs = new PropertyChangingEventArgs(String.Empty);
	
	private int _idCA;
	
	private string _dni;
	
	private System.Nullable<int> _codcur;
	
	private System.Nullable<int> _np;
	
	private EntityRef<Curso> _Curso;
	
	private EntityRef<Matricula> _Matricula;
	
    #region Definiciones de métodos de extensibilidad
    partial void OnLoaded();
    partial void OnValidate(System.Data.Linq.ChangeAction action);
    partial void OnCreated();
    partial void OnidCAChanging(int value);
    partial void OnidCAChanged();
    partial void OndniChanging(string value);
    partial void OndniChanged();
    partial void OncodcurChanging(System.Nullable<int> value);
    partial void OncodcurChanged();
    partial void OnnpChanging(System.Nullable<int> value);
    partial void OnnpChanged();
    #endregion
	
	public matCur()
	{
		this._Curso = default(EntityRef<Curso>);
		this._Matricula = default(EntityRef<Matricula>);
		OnCreated();
	}
	
	[global::System.Data.Linq.Mapping.ColumnAttribute(Storage="_idCA", AutoSync=AutoSync.OnInsert, DbType="Int NOT NULL IDENTITY", IsPrimaryKey=true, IsDbGenerated=true)]
	public int idCA
	{
		get
		{
			return this._idCA;
		}
		set
		{
			if ((this._idCA != value))
			{
				this.OnidCAChanging(value);
				this.SendPropertyChanging();
				this._idCA = value;
				this.SendPropertyChanged("idCA");
				this.OnidCAChanged();
			}
		}
	}
	
	[global::System.Data.Linq.Mapping.ColumnAttribute(Storage="_dni", DbType="NVarChar(8)")]
	public string dni
	{
		get
		{
			return this._dni;
		}
		set
		{
			if ((this._dni != value))
			{
				if (this._Matricula.HasLoadedOrAssignedValue)
				{
					throw new System.Data.Linq.ForeignKeyReferenceAlreadyHasValueException();
				}
				this.OndniChanging(value);
				this.SendPropertyChanging();
				this._dni = value;
				this.SendPropertyChanged("dni");
				this.OndniChanged();
			}
		}
	}
	
	[global::System.Data.Linq.Mapping.ColumnAttribute(Storage="_codcur", DbType="Int")]
	public System.Nullable<int> codcur
	{
		get
		{
			return this._codcur;
		}
		set
		{
			if ((this._codcur != value))
			{
				if (this._Curso.HasLoadedOrAssignedValue)
				{
					throw new System.Data.Linq.ForeignKeyReferenceAlreadyHasValueException();
				}
				this.OncodcurChanging(value);
				this.SendPropertyChanging();
				this._codcur = value;
				this.SendPropertyChanged("codcur");
				this.OncodcurChanged();
			}
		}
	}
	
	[global::System.Data.Linq.Mapping.ColumnAttribute(Storage="_np", DbType="Int")]
	public System.Nullable<int> np
	{
		get
		{
			return this._np;
		}
		set
		{
			if ((this._np != value))
			{
				this.OnnpChanging(value);
				this.SendPropertyChanging();
				this._np = value;
				this.SendPropertyChanged("np");
				this.OnnpChanged();
			}
		}
	}
	
	[global::System.Data.Linq.Mapping.AssociationAttribute(Name="Curso_matCur", Storage="_Curso", ThisKey="codcur", OtherKey="CodCur", IsForeignKey=true)]
	public Curso Curso
	{
		get
		{
			return this._Curso.Entity;
		}
		set
		{
			Curso previousValue = this._Curso.Entity;
			if (((previousValue != value) 
						|| (this._Curso.HasLoadedOrAssignedValue == false)))
			{
				this.SendPropertyChanging();
				if ((previousValue != null))
				{
					this._Curso.Entity = null;
					previousValue.matCur.Remove(this);
				}
				this._Curso.Entity = value;
				if ((value != null))
				{
					value.matCur.Add(this);
					this._codcur = value.CodCur;
				}
				else
				{
					this._codcur = default(Nullable<int>);
				}
				this.SendPropertyChanged("Curso");
			}
		}
	}
	
	[global::System.Data.Linq.Mapping.AssociationAttribute(Name="Matricula_matCur", Storage="_Matricula", ThisKey="dni", OtherKey="dni", IsForeignKey=true)]
	public Matricula Matricula
	{
		get
		{
			return this._Matricula.Entity;
		}
		set
		{
			Matricula previousValue = this._Matricula.Entity;
			if (((previousValue != value) 
						|| (this._Matricula.HasLoadedOrAssignedValue == false)))
			{
				this.SendPropertyChanging();
				if ((previousValue != null))
				{
					this._Matricula.Entity = null;
					previousValue.matCur.Remove(this);
				}
				this._Matricula.Entity = value;
				if ((value != null))
				{
					value.matCur.Add(this);
					this._dni = value.dni;
				}
				else
				{
					this._dni = default(string);
				}
				this.SendPropertyChanged("Matricula");
			}
		}
	}
	
	public event PropertyChangingEventHandler PropertyChanging;
	
	public event PropertyChangedEventHandler PropertyChanged;
	
	protected virtual void SendPropertyChanging()
	{
		if ((this.PropertyChanging != null))
		{
			this.PropertyChanging(this, emptyChangingEventArgs);
		}
	}
	
	protected virtual void SendPropertyChanged(String propertyName)
	{
		if ((this.PropertyChanged != null))
		{
			this.PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
		}
	}
}

[global::System.Data.Linq.Mapping.TableAttribute(Name="dbo.Matricula")]
public partial class Matricula : INotifyPropertyChanging, INotifyPropertyChanged
{
	
	private static PropertyChangingEventArgs emptyChangingEventArgs = new PropertyChangingEventArgs(String.Empty);
	
	private string _dni;
	
	private string _nombre;
	
	private string _semestre;
	
	private System.Nullable<int> _ncuotas;
	
	private string _cancelo;
	
	private EntitySet<matCur> _matCur;
	
    #region Definiciones de métodos de extensibilidad
    partial void OnLoaded();
    partial void OnValidate(System.Data.Linq.ChangeAction action);
    partial void OnCreated();
    partial void OndniChanging(string value);
    partial void OndniChanged();
    partial void OnnombreChanging(string value);
    partial void OnnombreChanged();
    partial void OnsemestreChanging(string value);
    partial void OnsemestreChanged();
    partial void OnncuotasChanging(System.Nullable<int> value);
    partial void OnncuotasChanged();
    partial void OncanceloChanging(string value);
    partial void OncanceloChanged();
    #endregion
	
	public Matricula()
	{
		this._matCur = new EntitySet<matCur>(new Action<matCur>(this.attach_matCur), new Action<matCur>(this.detach_matCur));
		OnCreated();
	}
	
	[global::System.Data.Linq.Mapping.ColumnAttribute(Storage="_dni", DbType="NVarChar(8) NOT NULL", CanBeNull=false, IsPrimaryKey=true)]
	public string dni
	{
		get
		{
			return this._dni;
		}
		set
		{
			if ((this._dni != value))
			{
				this.OndniChanging(value);
				this.SendPropertyChanging();
				this._dni = value;
				this.SendPropertyChanged("dni");
				this.OndniChanged();
			}
		}
	}
	
	[global::System.Data.Linq.Mapping.ColumnAttribute(Storage="_nombre", DbType="NVarChar(50)")]
	public string nombre
	{
		get
		{
			return this._nombre;
		}
		set
		{
			if ((this._nombre != value))
			{
				this.OnnombreChanging(value);
				this.SendPropertyChanging();
				this._nombre = value;
				this.SendPropertyChanged("nombre");
				this.OnnombreChanged();
			}
		}
	}
	
	[global::System.Data.Linq.Mapping.ColumnAttribute(Storage="_semestre", DbType="NVarChar(10)")]
	public string semestre
	{
		get
		{
			return this._semestre;
		}
		set
		{
			if ((this._semestre != value))
			{
				this.OnsemestreChanging(value);
				this.SendPropertyChanging();
				this._semestre = value;
				this.SendPropertyChanged("semestre");
				this.OnsemestreChanged();
			}
		}
	}
	
	[global::System.Data.Linq.Mapping.ColumnAttribute(Storage="_ncuotas", DbType="Int")]
	public System.Nullable<int> ncuotas
	{
		get
		{
			return this._ncuotas;
		}
		set
		{
			if ((this._ncuotas != value))
			{
				this.OnncuotasChanging(value);
				this.SendPropertyChanging();
				this._ncuotas = value;
				this.SendPropertyChanged("ncuotas");
				this.OnncuotasChanged();
			}
		}
	}
	
	[global::System.Data.Linq.Mapping.ColumnAttribute(Storage="_cancelo", DbType="NVarChar(1)")]
	public string cancelo
	{
		get
		{
			return this._cancelo;
		}
		set
		{
			if ((this._cancelo != value))
			{
				this.OncanceloChanging(value);
				this.SendPropertyChanging();
				this._cancelo = value;
				this.SendPropertyChanged("cancelo");
				this.OncanceloChanged();
			}
		}
	}
	
	[global::System.Data.Linq.Mapping.AssociationAttribute(Name="Matricula_matCur", Storage="_matCur", ThisKey="dni", OtherKey="dni")]
	public EntitySet<matCur> matCur
	{
		get
		{
			return this._matCur;
		}
		set
		{
			this._matCur.Assign(value);
		}
	}
	
	public event PropertyChangingEventHandler PropertyChanging;
	
	public event PropertyChangedEventHandler PropertyChanged;
	
	protected virtual void SendPropertyChanging()
	{
		if ((this.PropertyChanging != null))
		{
			this.PropertyChanging(this, emptyChangingEventArgs);
		}
	}
	
	protected virtual void SendPropertyChanged(String propertyName)
	{
		if ((this.PropertyChanged != null))
		{
			this.PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
		}
	}
	
	private void attach_matCur(matCur entity)
	{
		this.SendPropertyChanging();
		entity.Matricula = this;
	}
	
	private void detach_matCur(matCur entity)
	{
		this.SendPropertyChanging();
		entity.Matricula = null;
	}
}

[global::System.Data.Linq.Mapping.TableAttribute(Name="dbo.AlumnosCursosProfesores")]
public partial class AlumnosCursosProfesores
{
	
	private string _DNI;
	
	private string _Alumno;
	
	private string _Semestre;
	
	private string _Curso;
	
	private string _Profesor;
	
	public AlumnosCursosProfesores()
	{
	}
	
	[global::System.Data.Linq.Mapping.ColumnAttribute(Storage="_DNI", DbType="NVarChar(8) NOT NULL", CanBeNull=false)]
	public string DNI
	{
		get
		{
			return this._DNI;
		}
		set
		{
			if ((this._DNI != value))
			{
				this._DNI = value;
			}
		}
	}
	
	[global::System.Data.Linq.Mapping.ColumnAttribute(Storage="_Alumno", DbType="NChar(20)")]
	public string Alumno
	{
		get
		{
			return this._Alumno;
		}
		set
		{
			if ((this._Alumno != value))
			{
				this._Alumno = value;
			}
		}
	}
	
	[global::System.Data.Linq.Mapping.ColumnAttribute(Storage="_Semestre", DbType="NVarChar(10)")]
	public string Semestre
	{
		get
		{
			return this._Semestre;
		}
		set
		{
			if ((this._Semestre != value))
			{
				this._Semestre = value;
			}
		}
	}
	
	[global::System.Data.Linq.Mapping.ColumnAttribute(Storage="_Curso", DbType="NVarChar(50)")]
	public string Curso
	{
		get
		{
			return this._Curso;
		}
		set
		{
			if ((this._Curso != value))
			{
				this._Curso = value;
			}
		}
	}
	
	[global::System.Data.Linq.Mapping.ColumnAttribute(Storage="_Profesor", DbType="NVarChar(50)")]
	public string Profesor
	{
		get
		{
			return this._Profesor;
		}
		set
		{
			if ((this._Profesor != value))
			{
				this._Profesor = value;
			}
		}
	}
}

public partial class alumnos_cursoResult
{
	
	private int _idCA;
	
	private string _dni;
	
	private System.Nullable<int> _codcur;
	
	private System.Nullable<int> _np;
	
	private string _curso;
	
	private string _dni1;
	
	private string _nombre;
	
	private string _apellido;
	
	public alumnos_cursoResult()
	{
	}
	
	[global::System.Data.Linq.Mapping.ColumnAttribute(Storage="_idCA", DbType="Int NOT NULL")]
	public int idCA
	{
		get
		{
			return this._idCA;
		}
		set
		{
			if ((this._idCA != value))
			{
				this._idCA = value;
			}
		}
	}
	
	[global::System.Data.Linq.Mapping.ColumnAttribute(Storage="_dni", DbType="NVarChar(8)")]
	public string dni
	{
		get
		{
			return this._dni;
		}
		set
		{
			if ((this._dni != value))
			{
				this._dni = value;
			}
		}
	}
	
	[global::System.Data.Linq.Mapping.ColumnAttribute(Storage="_codcur", DbType="Int")]
	public System.Nullable<int> codcur
	{
		get
		{
			return this._codcur;
		}
		set
		{
			if ((this._codcur != value))
			{
				this._codcur = value;
			}
		}
	}
	
	[global::System.Data.Linq.Mapping.ColumnAttribute(Storage="_np", DbType="Int")]
	public System.Nullable<int> np
	{
		get
		{
			return this._np;
		}
		set
		{
			if ((this._np != value))
			{
				this._np = value;
			}
		}
	}
	
	[global::System.Data.Linq.Mapping.ColumnAttribute(Storage="_curso", DbType="NVarChar(50)")]
	public string curso
	{
		get
		{
			return this._curso;
		}
		set
		{
			if ((this._curso != value))
			{
				this._curso = value;
			}
		}
	}
	
	[global::System.Data.Linq.Mapping.ColumnAttribute(Storage="_dni1", DbType="NVarChar(8) NOT NULL", CanBeNull=false)]
	public string dni1
	{
		get
		{
			return this._dni1;
		}
		set
		{
			if ((this._dni1 != value))
			{
				this._dni1 = value;
			}
		}
	}
	
	[global::System.Data.Linq.Mapping.ColumnAttribute(Storage="_nombre", DbType="NChar(20)")]
	public string nombre
	{
		get
		{
			return this._nombre;
		}
		set
		{
			if ((this._nombre != value))
			{
				this._nombre = value;
			}
		}
	}
	
	[global::System.Data.Linq.Mapping.ColumnAttribute(Storage="_apellido", DbType="NVarChar(20)")]
	public string apellido
	{
		get
		{
			return this._apellido;
		}
		set
		{
			if ((this._apellido != value))
			{
				this._apellido = value;
			}
		}
	}
}
#pragma warning restore 1591
